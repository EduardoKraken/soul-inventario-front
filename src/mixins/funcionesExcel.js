import Vue from 'vue'
import { mapGetters,mapActions } from 'vuex';

var XLSX = require("xlsx");

export default {
	methods: {
    exportExcel(dataExport, name){
      let data = XLSX.utils.json_to_sheet(dataExport)
      const workbook = XLSX.utils.book_new()
      const filename = name
      XLSX.utils.book_append_sheet(workbook, data, filename)

      const excelData = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });

      const blob = new Blob([excelData], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      
      const url = URL.createObjectURL(blob);
      
      // Crea un enlace de descarga
      const link = document.createElement('a');
      link.href = url;
      link.download = filename + '.xlsx';
      
      // Simula un clic en el enlace de descarga
      link.click();
      
      // Limpia la URL del objeto Blob
      URL.revokeObjectURL(url);
    },
  }
}