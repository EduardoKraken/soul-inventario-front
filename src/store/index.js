import Vue from 'vue'
import Vuex         from 'vuex'
import UsuarioCapa  from '@/modules/usuariocapa'

import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    UsuarioCapa,
  },
  plugins: [createPersistedState()]
})