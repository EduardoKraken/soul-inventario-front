import Vue from 'vue'
import VueRouter from 'vue-router'


import Login          from '@/views/usuario/Login.vue'
import Home           from '@/views/Home.vue'

import Usuarios       from '@/views/catalogos/Usuarios.vue'
import Articulos      from '@/views/catalogos/Articulos.vue'
import Unidades       from '@/views/catalogos/Unidades.vue'
import Puestos        from '@/views/catalogos/Puestos.vue'

import Entradas       from '@/views/almacen/Entradas.vue'
import Salidas        from '@/views/almacen/Salidas.vue'
import Almacen        from '@/views/almacen/Almacen.vue'
import Discrepancias  from '@/views/almacen/Discrepancias.vue'


import Proyectos      from '@/views/produccion/Proyectos.vue'
import Etapas         from '@/views/produccion/Etapas.vue'
import CentroCalidad  from '@/views/produccion/CentroCalidad.vue'
import Progresos      from '@/views/produccion/Progresos.vue'

import store      from '@/store'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: '',
  base: process.env.BASE_URL,
  routes: [

    { path: '/'          , name: 'Login'                , component: Login          , meta: { libre: true }},
    { path: '/home'      , name: 'Home'                 , component: Home           , meta: { libre: true }},
    { path: '/usuarios'  , name: 'Usuarios'             , component: Usuarios       , meta: { libre: true }},

    { path: '/articulos'  , name: 'Articulos'           , component: Articulos      , meta: { libre: true }},
    { path: '/unidades'   , name: 'Unidades'            , component: Unidades       , meta: { libre: true }},
    { path: '/puestos'    , name: 'Puestos'             , component: Puestos        , meta: { libre: true }},

    { path: '/entradas'       , name: 'Entradas'        , component: Entradas       , meta: { libre: true }},
    { path: '/salidas'        , name: 'Salidas'         , component: Salidas        , meta: { libre: true }},
    { path: '/almacen'        , name: 'Almacen'         , component: Almacen        , meta: { libre: true }},
    { path: '/discrepancias'  , name: 'Discrepancias'   , component: Discrepancias  , meta: { libre: true }},

    { path: '/proyectos'      , name: 'Proyectos'       , component: Proyectos      , meta: { libre: true }},
    { path: '/etapas'         , name: 'Etapas'          , component: Etapas         , meta: { libre: true }},
    { path: '/centrocalidad'  , name: 'CentroCalidad'   , component: CentroCalidad  , meta: { libre: true }},
    { path: '/progresos'      , name: 'Progresos'       , component: Progresos      , meta: { libre: true }},

  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.libre)) {
    next()
  } else if (store.state.UsuarioCapa.datosUsuarioCapa) {
    if (to.matched.some(record => record.meta.USUARIO)) {
      next()
    }
  } else {
    next({
      name: 'Login'
    })
  }
})

export default router